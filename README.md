MRMFinder documentation
=============

The MRMFinder service contains the python3 script *mrmfinder.py* which is the
script of the latest version of the MRMFinder service. MRMFinder identifies
genes of interest in Plasmodium related to malaria, and the counts of all mutated
bases at each position, related or not to resistance to drugs.


## Content of the repository
1. mrmfinder.py - the program
2. test - folder with test data

## Installation

Setting up MRMFinder script and database
```bash
# Go to wanted location for mrmfinder
cd /path/to/some/dir

# Clone and enter the mrmfinder directory
git clone https://git@bitbucket.org/genomicepidemiology/mrmfinder.git
cd mrmfinder

# Installing up the MRMFinder database
# Go to wanted location for mrmfinder database
cd /path/to/some/dir

# Clone and enter the mrmfinder directory
git clone https://git@bitbucket.org/genomicepidemiology/mrmfinder_db.git
cd mrmfinder_db

```

### Installing dependencies (for python script):

#### Download BioPython
```url
http://biopython.org/DIST/docs/install/Installation.html
```
#### Install the cgecore module to python3
```bash
pip3 install cgecore
```
#### Install KMA
If kma/kma_index has no been installed please install kma_index from the kma repository: https://bitbucket.org/genomicepidemiology/kma

##Download and install MRMFinder database

```bash
# Go to the directory where you want to store the mrmfinder database
cd /path/to/some/dir
# Clone database from git repository (develop branch)
git clone https://bitbucket.org/genomicepidemiology/mrmfinder_db.git
cd mrmfinder_db
# Install MRMFinder database with executable kma_index program
python3 INSTALL.py kma_index
```
## Usage

You can run mrmfinder command line using python3

```bash

# Example of running mrmfinder
python3 mrmfinder.py -i ./test/1_S1_L001_R1_001.fastq.gz -o . -p /path/to/resfinder_db \
-mp /path/to/kma -s P_falciparum -t 0.10 -l 0.10 -d 3 -c 0.1 -x

# The program can be invoked with the -h option
usage: mrmfinder.py [-h] [-i INFILE [INFILE ...]] [-o OUTDIR] [-tmp TMP_DIR]
                    [-mp METHOD_PATH] [-p DB_PATH] [-l MIN_COV]
                    [-t IDENTITY_THRESHOLD] [-d DEPTH_THRESHOLD]
                    [-c MIN_ABUNDANCE] [-s SPECIE] [-x] [-q]

optional arguments:
  -h, --help            show this help message and exit
  -i INFILE [INFILE ...], --infile INFILE [INFILE ...]
                        FASTA or FASTQ input files.
  -o OUTDIR, --outputPath OUTDIR
                        Path for output files
  -tmp TMP_DIR, --tmp_dir TMP_DIR
                        Temporary directory for storage of the results from
                        the external software.
  -mp METHOD_PATH, --methodPath METHOD_PATH
                        Path to executable kma
  -p DB_PATH, --databasePath DB_PATH
                        Path to the databases
  -l MIN_COV, --mincov MIN_COV
                        Minimum coverage
  -t IDENTITY_THRESHOLD, --identityThreshold IDENTITY_THRESHOLD
                        Minimum threshold for identity
  -d DEPTH_THRESHOLD, --depthThreshold DEPTH_THRESHOLD
                        Minimum threshold for depth
  -c MIN_ABUNDANCE, --minAbundance MIN_ABUNDANCE
                        Minimum abundance of mutation for reporting
  -s SPECIE, --specie SPECIE
                        Specie of plasmodium
  -x, --extented_output
                        Give extented output with allignment files, template
                        and query hits in fasta and a tab seperated file with
                        allele profile results
  -q, --quiet
```

### Web-server

A webserver implementing the methods is available at the [CGE
website](http://www.genomicepidemiology.org/) and can be found here:
https://cge.cbs.dtu.dk/services/MRMFinder/

### Documentation

The documentation available as of the date of this release can be found at
https://bitbucket.org/genomicepidemiology/mrmfinder/overview.


Citation
=======


License
=======

Copyright (c) 2014, Ole Lund, Technical University of Denmark
All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
