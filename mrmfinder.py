#!/usr/bin/env python3
from __future__ import division
from argparse import ArgumentParser
from tabulate import tabulate
from cgecore.blaster import Blaster
from cgecore.cgefinder import CGEFinder
#from cgefinder import CGEFinder
from distutils.spawn import find_executable
import sys, os, time, re, subprocess
import json, gzip, pprint, math
import numpy as np
import Bio
import time as time_lib
import itertools
import pandas as pd

##########################################################################
# FUNCTIONS
##########################################################################

class MRMFinder(CGEFinder):

    def __init__(self, db_config_file, db_path, method_path, species):
        self.db_conf_file = db_config_file
        self.db_path = db_path
        self.method_path = method_path
        self.species = str(species)

    def kma_run(self, tmp_dir, min_cov, identity_threshold, infile):
        """Checks file format and number of files and runs kma."""

        self.tmp_dir = tmp_dir
        self.identity_threshold = identity_threshold

        # Check file format (fasta, fastq or other format)
        self.file_format = MRMFinder.get_file_format(infile)

        if self.file_format == "fastq" or self.file_format == "fasta":
            if not self.method_path:
                self.method_path = "kma"
            if find_executable(self.method_path) == None:
                sys.exit("No valid path to a kma program was provided. Use the -mp flag to provide the path.")

            # Check the number of files
            if len(infile) == 1:
                infile_1 = infile[0]
                infile_2 = None
            elif len(infile) == 2:
                infile_1 = infile[0]
                infile_2 = infile[1]
            else:
                sys.exit("Only 2 input file accepted for raw read data,\
                         if data from more runs is avaliable for the same\
                         sample, please concatinate the reads into two files")

        else:
            sys.exit("Input file must be fastq or fasta format, not "+ self.file_format)

        self.sample_name = os.path.basename(sorted(args.infile)[0])
        method_obj = CGEFinder.kma(infile_1, tmp_dir, [self.species], self.db_path, min_cov=min_cov,
                              threshold=identity_threshold, kma_path=self.method_path, sample_name=self.sample_name,
                              inputfile_2=infile_2, kma_mrs=0.75, kma_gapopen=-5,
                              kma_gapextend=-1, kma_penalty=-3, kma_reward=1,
                              kma_add_args = "-matrix -1t1 -dense -mp 10 -k 21")

        self.kma_results = method_obj.results
        self.query_aligns = method_obj.gene_align_query
        self.homo_aligns  = method_obj.gene_align_homo
        self.sbjct_aligns = method_obj.gene_align_sbjct

    def make_gene_names_dict(self):
        """
        Makes a dict with gene ID as key and gene name as value
        based on the database config file.
        """
        # Get gene names in dict
        self.gene_names = {}
        with open(self.db_conf_file, "r") as config_file:
            for line in config_file:
                line = line.strip()
                if line.startswith("#") or line == "":
                    continue
                gene_info = line.split("\t")
                if gene_info[0] not in self.gene_names:
                    self.gene_names[gene_info[0]] = {}
                self.gene_names[gene_info[0]][gene_info[1]] = gene_info[2]

    def make_mutation_dict(self):
        """
        This function opens the file "known_mutations.tsv", and reads the
        content into a dict. The dict will contain information about
        all known mutations leading to resistance.
        """

        mut_db_path = self.db_path + "/known_mutations.tsv"

        try:
            drugfile = open(mut_db_path, "r")
        except:
            sys.exit("Wrong path: %s"%(mut_db_path))

        self.resistance_dict = dict()
        for line in drugfile:

            if line.startswith("#"):
                continue

            line = (line.strip()).split('\t')
            gene_id = line[0]
            mutation = dict()

            # Only consider mutations in genes found in the gene list
            if gene_id in self.gene_names[self.species]:
                mutation['gene_name'] = line[1]
                mutation['codon_pos'] = int(line[2])
                mutation['ref_codon'] = line[4]
                mutation['alt_codon'] = line[5]
                mutation['ref_aa'] = line[8]
                mutation['alt_aa'] = line[9]
                mutation['drug'] = line[10]

                nuc_pos = int(line[3])
                alt_nuc = line[6].split(">")[1]
                if gene_id not in self.resistance_dict:
                    self.resistance_dict[gene_id] = {}

                if nuc_pos not in self.resistance_dict[gene_id]:
                    self.resistance_dict[gene_id][nuc_pos] = {}

                self.resistance_dict[gene_id][nuc_pos][alt_nuc] = mutation
        drugfile.close()

    @staticmethod
    def text_table(headers, rows, empty_replace='-'):
        ''' Create text table

        USAGE:
            >>> from tabulate import tabulate
            >>> headers = ['A','B']
            >>> rows = [[1,2],[3,4]]
            >>> print(text_table(headers, rows))
            **********
            A    B
            **********
            1    2
            3    4
            ==========
        '''
        # Replace empty cells with placeholder
        rows = map(lambda row: map(lambda x: x if x else empty_replace, row), rows)
        # Create table
        table = tabulate(rows, headers, tablefmt='simple').split('\n')
        # Prepare title injection
        width = len(table[0])
        # Switch horisontal line
        seperator = '*'*(width+2)
        table[1] = '*'*(width+2)
        # Update table with title
        table = ("%s\n"*3)%('*'*(width+2), '\n'.join(table), '='*(width+2))
        return table, seperator

    @staticmethod
    def is_gzipped(file_path):
        ''' Returns True if file is gzipped and False otherwise.
            The result is inferred from the first two bits in the file read
            from the input path.
            On unix systems this should be: 1f 8b
            Theoretically there could be exceptions to this test but it is
            unlikely and impossible if the input files are otherwise expected
            to be encoded in utf-8.
        '''
        with open(file_path, mode='rb') as fh:
            bit_start = fh.read(2)
        if(bit_start == b'\x1f\x8b'):
            return True
        else:
            return False

    @staticmethod
    def get_file_format(input_files):
        """
        Takes all input files and checks their first character to assess
        the file format. Returns one of the following strings; fasta, fastq,
        other or mixed. fasta and fastq indicates that all input files are
        of the same format, either fasta or fastq. other indiates that all
        files are not fasta nor fastq files. mixed indicates that the inputfiles
        are a mix of different file formats.
        Is called from kma()
        """

        # Open all input files and get the first character
        file_format = []
        invalid_files = []
        for infile in input_files:
            if MRMFinder.is_gzipped(infile):
                f = gzip.open(infile, "rb")
                fst_char = f.read(1);
            else:
                f = open(infile, "rb")
                fst_char = f.read(1);
            f.close()
            # Assess the first character
            if fst_char == b"@":
                file_format.append("fastq")
            elif fst_char == b">":
                file_format.append("fasta")
            else:
                invalid_files.append("other")
        if len(set(file_format)) != 1:
            return "mixed"
        return ",".join(set(file_format))

    def find_triplet(self, triplet_index, gene,nuc_count, nuc_calls,ref_trip):
        """Find the possible triplets in the query sequence to report the impossible
        mutated codons"""
        pos_line = {0:"A",1:"C",2:"G",3:"T",4:"N",5:"-"}
        trip_list = []
        for tri in triplet_index:
            line_tri = self.mutated_dict[gene][self.mutated_dict[gene][:,-1]==str(float(tri)),:]
            pos_mut = np.nonzero((line_tri[0][1:-1].astype('float')/sum(line_tri[0][1:-1].astype('float')))>self.min_abundance)
            if len(pos_mut[0])!=0:
                nuc_mut = np.vectorize(pos_line.get)(pos_mut)[0]
            else:
                nuc_mut = np.array(["N","a","N"])
            trip_list.append(nuc_mut.tolist())
        trip_comb=[]
        aa_comb=[]
        for x in list(itertools.product(*trip_list)):
            x=''.join(x)
            if x == ref_trip:
                continue
            trip_comb.append(x)
            if "-" not in x:
                aa_comb.append(Bio.Seq.translate(x))
            else:
                aa_comb.append("Gap")
        return trip_comb, list(set(aa_comb))

    def get_aa(self, list_nuc):
        triplet_list = []
        aa_list = []
        for nuc in list(itertools.product(*list_nuc)):
            nuc = ''.join(nuc)
            triplet_list.append(nuc)
            if "-" not in nuc:
                aa_list.append(Bio.Seq.translate(nuc))
            else:
                aa_list.append("Gap")
        return triplet_list, aa_list

    def find_mutations(self, depth_threshold,min_abundance):
        """Reads kma matrix file to check for mutations. Calls save_mutation()
        which saves mutations in either known_muts or unknown_muts."""
        self.depth_threshold = depth_threshold
        self.min_abundance = min_abundance
        nuc_pos = np.array(["A","C","G","T","N","-"])
        gene = ""
        matrix_file = "{}/kma_{}_{}.mat.gz".format(self.tmp_dir, self.species, self.sample_name)
        matrix_file = gzip.open(matrix_file, "rb")
        snp_frame = pd.DataFrame(columns=["Gene","Codon","Pos","Ref","A","C","G","T","N","-"])
        mat_frames = {}
        mut_frames = {}
        for line in matrix_file:
            line = line.decode("ascii").rstrip()
            if line.startswith("#"):
                gene_id = line.lstrip("#")
                gene = self.gene_names[self.species][gene_id]
                mat_frames[gene_id] = pd.DataFrame(columns=["Gene","Codon","Pos_in_Tri","Pos","Ref", "A","C","G","T","N","-","Depth","Mutated","Ref_codon","Mut_codon","Ref_aa","Mut_aa"])
                nuc_count = 0
            elif line != "":
                line=line.split("\t")
                if line != "-":
                    nuc_count += 1
                codon_pos = math.ceil(float(nuc_count)/3)
                depth = sum(np.array(line[1:], dtype=int))
                nclt_series = pd.Series(line[1:],index=["A","C","G","T","N","-"], dtype="int")
                nclt_series = nclt_series[nclt_series!=0]/depth
                mutated_depths = nclt_series.loc[~nclt_series.index.isin([line[0]])]
                pos_in_triplet=((float(codon_pos)*3-float(nuc_count))-3)*-1
                if pos_in_triplet == 1:
                    codon_dict = {}
                    ref_trip = []
                    empty_triplet = 0
                if nclt_series.empty:
                    empty_triplet = 1
                codon_dict[pos_in_triplet] = nclt_series.to_dict()
                ref_trip.append(line[0])
                if pos_in_triplet == 3:
                    if empty_triplet != 1:
                        trip_list=[list(codon_dict[1].keys()),list(codon_dict[2].keys()),list(codon_dict[3].keys())]
                        freq_list=[list(codon_dict[1].values()),list(codon_dict[2].values()),list(codon_dict[3].values())]
                        triplet_list, aa_list = self.get_aa(trip_list)
                        ref_triplet, aa_ref = self.get_aa(ref_trip)
                        ref_triplet = ref_triplet[0]
                        aa_ref = aa_ref[0]
                        triplet_freq = []
                        for freq in list(itertools.product(*freq_list)):
                            triplet_freq.append(np.prod(freq))
                        triplet_dict = dict(zip(triplet_list,triplet_freq))
                        aa_dict = {}
                        for x,y in zip(aa_list,triplet_freq):
                            if x not in aa_dict:
                                aa_dict[x] = y
                            else:
                                aa_dict[x] = aa_dict[x]+y
                    else:
                        triplet_dict,ref_triplet,aa_dict,aa_ref = ["NaN"]*4
                    mat_frames[gene_id].loc[mat_frames[gene_id]["Pos"]==nuc_count-1,["Ref_codon","Mut_codon","Ref_aa","Mut_aa"]]=[[ref_triplet, triplet_dict, aa_ref, aa_dict]]
                    mat_frames[gene_id].loc[mat_frames[gene_id]["Pos"]==nuc_count-2,["Ref_codon","Mut_codon","Ref_aa","Mut_aa"]]=[[ref_triplet, triplet_dict, aa_ref, aa_dict]]
                if depth > 0:
                    series_snp = pd.Series([gene,codon_pos,pos_in_triplet,nuc_count]+line+[depth,0,ref_triplet,triplet_dict,aa_ref,aa_dict], index=mat_frames[gene_id].columns)
                    if np.any((mutated_depths/depth)>self.min_abundance) and depth > self.depth_threshold:
                        series_snp.loc["Mutated"] = 1
                        if gene_id in self.resistance_dict:
                            if nuc_count in self.resistance_dict[gene_id]:
                                key_nuc = list(self.resistance_dict[gene_id][nuc_count].keys())[0]
                                if sum(mutated_depths[key_nuc.split(",")].values > self.min_abundance) >=1:
                                    mutated_description = [self.resistance_dict[gene_id][nuc_count][key_nuc]["drug"],
                                                            str(self.resistance_dict[gene_id][nuc_count][key_nuc]["ref_aa"]+">"+self.resistance_dict[gene_id][nuc_count][key_nuc]["alt_aa"]),
                                                            str(self.resistance_dict[gene_id][nuc_count][key_nuc]["ref_codon"]+">"+self.resistance_dict[gene_id][nuc_count][key_nuc]["alt_codon"])]
                                    series_snp.loc["Mutated"] = mutated_description
                    mat_frames[gene_id] = mat_frames[gene_id].append(series_snp, ignore_index=True)
        snp_frames_list = []
        mut_frames_list = []
        for k,v in mat_frames.items():
            v = v.loc[v["Mutated"]!=0]
            if not v.empty:
                snp_frames_list.append(v)
                v = v.loc[v["Mutated"]!=1]
                if not v.empty:
                    v["Drug"]=v["Mutated"].str[0]
                    v["Triplet Mutation"]=v["Mutated"].str[2]
                    v["Aa Mutation"]=v["Mutated"].str[1]
                    mut_frames_list.append(v)
        snp_frames = pd.concat(snp_frames_list).reset_index(drop=True)
        mut_frames = pd.concat(mut_frames_list).reset_index(drop=True)
        self.snp_frame = snp_frames[["Gene","Pos","Ref", "A","C","G","T","N","-","Depth","Codon","Pos_in_Tri"]].astype(str).values.tolist()
        self.resistance_muts = mut_frames[["Gene","Pos","Ref", "A","C","G","T","N","-","Depth","Codon","Pos_in_Tri","Triplet Mutation","Aa Mutation","Drug"]].astype(str).values.tolist()
        matrix_file.close()

    def write_align(seq, seq_name, file_handle):
        """
        Writes alignment. Is called from make_aln().
        """
        file_handle.write("# {}".format(seq_name) + "\n")
        sbjct_seq = seq[0]
        homol_seq = seq[1]
        query_seq = seq[2]
        for i in range(0,len(sbjct_seq),60):
            file_handle.write("%-10s\t%s\n"%("template:", sbjct_seq[i:i+60]))
            file_handle.write("%-10s\t%s\n"%("", homol_seq[i:i+60]))
            file_handle.write("%-10s\t%s\n\n"%("query:", query_seq[i:i+60]))

    def make_aln(self, file_handle):
        """
        Writes alignment in "results.txt" file. Uses write_align().
        Is being called from extended_output()
        """
        for species,dbs_info in self.json_results.items():
            if isinstance(dbs_info, str):
                continue
            for db_name, gene_info in dbs_info.items():

                #  for gene_id, gene_info in sorted(db_info.items()):
                seq_name = db_name
                hit_name = gene_info["gene_id"]

                seqs = ["","",""]
                seqs[0] = self.sbjct_aligns[self.species][hit_name]
                seqs[1] = self.homo_aligns[self.species][hit_name]
                seqs[2] = self.query_aligns[self.species][hit_name]

                MRMFinder.write_align(seqs, seq_name, file_handle)

    def make_json_output(self):
        """
        Writes json output into "data.json" file.
        """
        json_results = dict()
        for spp in self.kma_results:
            if spp == 'excluded':
                continue

            json_results[spp] = {}

            if self.kma_results[spp] == "No hit found":
                json_results[spp] = "No hit found"
                continue

            for gene_id, gene_hit in self.kma_results[spp].items():
                db_name = self.gene_names[spp][gene_id]
                identity = gene_hit["perc_ident"]
                coverage = gene_hit["perc_coverage"]
                depth = gene_hit["depth"]

                # Skip hits below coverage
                if coverage < (min_cov*100) or identity < (identity_threshold*100):
                    continue
                if db_name not in json_results:
                    json_results[spp][db_name] = {}
                sbj_length = gene_hit["sbjct_length"]
                HSP = gene_hit["HSP_length"]
                positions_contig = "%s..%s"%(gene_hit["query_start"], gene_hit["query_end"])
                positions_ref = "%s..%s"%(gene_hit["sbjct_start"], gene_hit["sbjct_end"])

                # Write JSON results dict
                json_results[spp][db_name] = {"gene_id":gene_id,"identity":round(identity, 2),"HSP_length":HSP,
                                      "template_length":sbj_length,"position_in_ref":positions_ref,
                                      "coverage":round(coverage, 2),"depth":depth}
        # Get run info for JSON file
        self.service = os.path.basename(__file__).replace(".py", "")
        date = time_lib.strftime("%d.%m.%Y")    # TODO: time library not working
        time = time_lib.strftime("%H:%M:%S")

        # Make JSON output file
        data = {self.service:{}}

        userinput = {"filename(s)":args.infile, "method":"kma", "file_format":self.file_format}
        run_info = {"date":date, "time":time}

        data[self.service]["user_input"] = userinput
        data[self.service]["run_info"] = run_info
        data[self.service]["results"] = json_results

        # Save json output
        result_file = "{}/data.json".format(outdir)
        with open(result_file, "w") as outfile:
            json.dump(data, outfile)

        self.json_results = json_results

    def extended_output(self):
        """
        Makes the following files:
        - results_tab.tsv:
            Tab seperated file with known and unknown mutations
        - Hit_in_input_seq.fsa:
            Query sequence
        - Gene_seqs.fsa
            Subject sequence
        - results.txt
            Genes found and their identity, query/template length
            List of known mutations
            List of unknown mutations
            Alignments
        """

        header = ["Gene", "Identity", "Query / Template length", "Depth", "Position in reference", "Gene identifier"]
        snp_header = ["Gene", "Nucleotide position", "Ref. nucleotide","A","C","G","T","N","-", "Depth", "Codon position", "Position in triplet"]
        res_header = ["Gene", "Nucleotide position", "Ref. nucleotide","A","C","G","T","N","-", "Depth", "Codon position", "Position in triplet", "Triplet change", "Aminoacid change", "Drug"]

        # Define extented output
        table_filename_res  = "{}/results_resistance.tsv".format(outdir)
        table_filename_snp  = "{}/results_snp.tsv".format(outdir)
        query_filename  = "{}/Hit_in_input_seq.fsa".format(outdir)
        sbjct_filename  = "{}/Gene_seqs.fsa".format(outdir)
        result_filename = "{}/results.txt".format(outdir)
        table_res_file  = open(table_filename_res, "w")
        table_snp_file  = open(table_filename_snp, "w")
        query_file  = open(query_filename, "w")
        sbjct_file  = open(sbjct_filename, "w")
        result_file = open(result_filename, "w")

        # Make results file
        result_file.write("{} Results\n\nOrganism(s): {}\n\n".format(self.service, self.species))

        # Write tsv table
        rows = [["Database"] + header]
        for self.species, dbs_info in self.json_results.items():
            db_rows = []
            # Check if hits are found
            if isinstance(dbs_info, str):
                content = [''] * len(header)
                content[int(len(header) / 2)] = dbs_info
                db_rows.append(content)
                content = [''] * len(res_header)
                content[int(len(res_header) / 2)] = dbs_info
                self.resistance_muts = [content]
                content = [''] * len(snp_header)
                content[int(len(snp_header) / 2)] = dbs_info
                self.snp_frame = [content]
                continue

            for db_name, gene_info in sorted(dbs_info.items()):
                #for gene_name, gene_info in db_hits.items()
                gene_id = gene_info["gene_id"]
                identity = str(gene_info["identity"])
                coverage = str(gene_info["coverage"])
                depth = str(gene_info["depth"])
                template_HSP = str(gene_info["HSP_length"]) + " / " + str(gene_info["template_length"])
                position_in_ref = gene_info["position_in_ref"]

                # Add rows to result tables
                db_rows.append([db_name, identity, template_HSP, depth, position_in_ref, gene_id])
                rows.append([self.species, db_name, identity, template_HSP, position_in_ref, gene_id])

                # Write query fasta output
                query_seq = self.query_aligns[self.species][gene_id]
                sbjct_seq = self.sbjct_aligns[self.species][gene_id]

                if coverage == 100. and identity == 100.:
                    match = "PERFECT MATCH"
                else:
                    match = "WARNING"
                qry_header = ">{}:{} ID:{}% COV:{}% Best_match:{}\n".format(db_name, match, identity,
                                                                               coverage, gene_id)
                query_file.write(qry_header)
                for i in range(0,len(query_seq),60):
                    query_file.write(query_seq[i:i+60] + "\n")

                # Write template fasta output
                sbj_header = ">{}\n".format(gene_id)
                sbjct_file.write(sbj_header)
                for i in range(0,len(sbjct_seq),60):
                    sbjct_file.write(sbjct_seq[i:i+60] + "\n")

        if not self.resistance_muts:
            self.resistance_muts="No hit found"
            content = [''] * len(res_header)
            content[int(len(res_header) / 2)] = self.resistance_muts
            self.resistance_muts = [content]
        if not self.snp_frame:
            self.snp_frame="No hit found"
            content = [''] * len(snp_header)
            content[int(len(snp_header) / 2)] = self.snp_frame
            self.snp_frame = [content]
        if not db_rows:
            db_rows="No hit found"
            content = [''] * len(header)
            content[int(len(header) / 2)] = db_rows
            db_rows = [content]

        # Write db results tables in results file and table file
        table, seperator = MRMFinder.text_table(header, db_rows)
        result_file.write(seperator + "\n")
        result_file.write("Genes found\n")
        result_file.write(table + "\n")

        # Write known mutations table
        table, seperator = MRMFinder.text_table(res_header, self.resistance_muts)
        result_file.write(seperator + "\n")
        result_file.write("Known mutations\n")
        result_file.write(table + "\n")

        # Write unknown mutations table
        table, seperator = MRMFinder.text_table(snp_header, self.snp_frame)
        result_file.write(seperator + "\n")
        result_file.write("SNP variation\n")
        result_file.write(table + "\n")


        result_file.write("\n")
        table_res_file.write("\t".join(res_header) + "\n")
        table_snp_file.write("\t".join(snp_header) + "\n")

        if "No hit found" not in self.resistance_muts[0]:
            for row in self.resistance_muts:
                table_res_file.write("\t".join(row) + "\n")
        if "No hit found" not in self.snp_frame[0]:
            for row in self.snp_frame:
                table_snp_file.write("SNP variation\t" + "\t".join(row) + "\n")

        # Write allignment output
        result_file.write("\n\nExtended Output:\n\n")
        MRMFinder.make_aln(self, result_file)

        # Close all files
        query_file.close()
        sbjct_file.close()
        table_snp_file.close()
        table_res_file.close()
        result_file.close()

        if args.quiet:
            f.close()

if __name__ == '__main__':
    ##########################################################################
    # PARSE COMMAND LINE OPTIONS
    ##########################################################################

    parser = ArgumentParser()
    parser.add_argument("-i", "--infile", dest="infile", help="FASTA or FASTQ input files.", nargs = "+")
    parser.add_argument("-o", "--outputPath", dest="outdir",help="Path for output files", default='.')
    parser.add_argument("-tmp", "--tmp_dir", help="Temporary directory for storage of the results from the external software.")
    parser.add_argument("-mp", "--methodPath", dest="method_path",help="Path to executable kma")
    parser.add_argument("-p", "--databasePath", dest="db_path",help="Path to the databases", default='/database')
    parser.add_argument("-l", "--mincov", dest="min_cov",help="Minimum coverage", default=0.05)
    parser.add_argument("-t", "--identityThreshold", dest="identity_threshold",help="Minimum threshold for identity", default=0.1)
    parser.add_argument("-d", "--depthThreshold", dest="depth_threshold",help="Minimum threshold for depth", default=5)
    parser.add_argument("-c", "--minAbundance", dest="min_abundance",help="Minimum abundance of mutation for reporting", default=0.04)
    parser.add_argument("-s", "--specie", dest="specie", help="Specie of plasmodium", default="P_falciparum")
    parser.add_argument("-x", "--extented_output",
                        help="Give extented output with allignment files, template and query hits in fasta and\
                              a tab seperated file with allele profile results", action="store_true")
    parser.add_argument("-q", "--quiet", action="store_true")

    args = parser.parse_args()

    ###########################################################################
    ###                              CHECK INPUT                            ###
    ###########################################################################

    if args.quiet:
       f = open('/dev/null', 'w')
       sys.stdout = f

    # Defining varibales
    min_cov = float(args.min_cov)
    identity_threshold = float(args.identity_threshold)
    depth_threshold = float(args.depth_threshold)
    min_abundance = float(args.min_abundance)
    method_path = args.method_path
    # Check if valid database is provided
    if args.db_path is None:
       sys.exit("Input Error: No database directory was provided!\n")
    elif not os.path.exists(args.db_path):
       sys.exit("Input Error: The specified database directory does not exist!\n")
    else:
       # Check existence of config file
       db_config_file = '%s/config'%(args.db_path)
       if not os.path.exists(db_config_file):
          sys.exit("Input Error: The database config file could not be "
                              "found!")
       db_path = args.db_path

    # Check if valid input files are provided
    if args.infile is None:
       sys.exit("Input Error: No input file was provided!\n")
    elif not os.path.exists(args.infile[0]):
       sys.exit("Input Error: Input file does not exist!\n")
    elif len(args.infile) > 1:
       if not os.path.exists(args.infile[1]):
          sys.exit("Input Error: Input file does not exist!\n")
       infile = args.infile
    else:
       infile = args.infile

    # Check if valid output directory is provided
    if not os.path.exists(args.outdir):
       sys.exit("Input Error: Output dirctory does not exist!\n")
    outdir = os.path.abspath(args.outdir)

    # Check if valid tmp directory is provided
    if args.tmp_dir:
       if not os.path.exists(args.tmp_dir):
          sys.exit("Input Error: Tmp dirctory, {}, does not exist!\n".format(args.tmp_dir))
       else:
          tmp_dir = os.path.abspath(args.tmp_dir)
    else:
       tmp_dir = outdir

    ################################################################################
    ###                              MAIN                                        ###
    ################################################################################

    finder = MRMFinder(db_config_file, db_path, method_path, args.specie)

    finder.kma_run(tmp_dir, min_cov, identity_threshold, infile)
    finder.make_gene_names_dict()
    finder.make_mutation_dict()
    finder.find_mutations(depth_threshold, min_abundance)
    finder.make_json_output()
    if args.extented_output:
        finder.extended_output()
